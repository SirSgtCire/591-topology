CSCI 591: Topology Semester Project
===================================

This repository contains all the files for our Spring 2016 semester project in CSCI 591-2 Computation Topology.
The topic of our project Topological Techniques for Pan-genome Analysis.

Generating Data
---------------

Given a [FASTA file](https://en.wikipedia.org/wiki/FASTA_format) containing the sequence data for a population of genomes, we use the algorithm from "Efficient construction of a compressed de bruijn graph for pan-genome analysis" to construct a pan-genome from the data.
The algorithm, which we'll refer to as E-SplitMEM, can be downloaded [here](http://www.uni-ulm.de/in/theo/research/seqana.html).
To compile E-SplitMEM you must first install the [SDSL library](https://github.com/simongog/sdsl).
Once E-SplitMEM is isntalled, it will not run to completion, but rather, will throw a memory error or silently fail.
This error can be fixed by running E-SplitMEM with the [Valgrind](http://valgrind.org/) profiling tool

    valgrind a3.x yeastgenomes_2.fa yeastgenomes_2 kfile.txt

The first argument is the FASTA file containing the raw sequence of the population, the second is the name you want to prefix the output files with, and the third is a file containing a list of `k` values you want to construct de Bruijn graphs for.
E-SplitMEM should produce two files for each `k` value: a [DOT file](https://en.wikipedia.org/wiki/DOT_%28graph_description_language%29) containing the de Bruijn graph and a `txt` file containing the start node id for each genome in the graph.

Running Code
------------

Our algorithms are implemented in Python 2.7.
Since they rely on third party libraries, it is recommended that you create a [Python Virtual Environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/) to confine these dependencies too.
The virtual environment can be created as follows

    virtualenv -p /usr/bin/python2.7 venv

You can then activate the environment

    . ./venv/bin/activate

All the dependencies are listed in the *requirements.txt* file, and so the virtual environment can be bootstrapped with the command

    (venv) pip install -r requirements.txt

Note, one of the dependencies is [PyGraphviz](https://pygraphviz.github.io/), which requires you to install [Graphviz](http://www.graphviz.org/) on your system before it can be successfully installed.

The *main.py* file contains code to read in a de Bruijn as a DOT file and construct a [networkx graph](https://networkx.readthedocs.org/en/stable/) data structure for that graph.
It can be run as follows

    (venv) ./main.py yeastgenomes_2.k50.dot yeastgenomes_2.fa

The first argument is the DOT file containing a de Bruijn graph construct for a FASTA file, the second argument.
