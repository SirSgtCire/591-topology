\documentclass[conference]{IEEEtran}

\usepackage{hyperref}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

% for writing theorems, definitions, conjectures, etc
\usepackage{amsthm}
\theoremstyle{plain}
\newtheorem{thm}{Theorem} % reset theorem numbering for each chapter
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\theoremstyle{conjecture}
\newtheorem{conj}{Conjecture}

% for writing algorithms
\usepackage{algorithm, algpseudocode}

% for subfigures
\usepackage{caption, subcaption}

% correct bad hyphenation here
\hyphenation{}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Topological Methods for Pan-genome Analysis}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations

\author{\IEEEauthorblockN{Alan Cleary\IEEEauthorrefmark{1},
Eric DiGiovine\IEEEauthorrefmark{2},
Daniel Salinas\IEEEauthorrefmark{3} and
Killian Smith\IEEEauthorrefmark{4}}
\IEEEauthorblockA{Department of Computer Science,\\
Montana State University\\
Bozeman, MT 59717\\
Email: \IEEEauthorrefmark{1}http://alancleary.github.io/\#contact,
\IEEEauthorrefmark{2}edigiovine75@gmail.com,
\IEEEauthorrefmark{3}daniel.salinas@msu.montana.edu,\\
\IEEEauthorrefmark{4}vikingsheepman@gmail.com}}

% conference papers do not typically use \thanks and this command
% is locked out in conference mode. If really needed, such as for
% the acknowledgment of grants, issue a \IEEEoverridecommandlockouts
% after \documentclass

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
%\author{\IEEEauthorblockN{Michael Shell\IEEEauthorrefmark{1},
%Homer Simpson\IEEEauthorrefmark{2},
%James Kirk\IEEEauthorrefmark{3}, 
%Montgomery Scott\IEEEauthorrefmark{3} and
%Eldon Tyrell\IEEEauthorrefmark{4}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}School of Electrical and Computer Engineering\\
%Georgia Institute of Technology,
%Atlanta, Georgia 30332--0250\\ Email: see http://www.michaelshell.org/contact.html}
%\IEEEauthorblockA{\IEEEauthorrefmark{2}Twentieth Century Fox, Springfield, USA\\
%Email: homer@thesimpsons.com}
%\IEEEauthorblockA{\IEEEauthorrefmark{3}Starfleet Academy, San Francisco, California 96678-2391\\
%Telephone: (800) 555--1212, Fax: (888) 555--1212}
%\IEEEauthorblockA{\IEEEauthorrefmark{4}Tyrell Inc., 123 Replicant Street, Los Angeles, California 90210--4321}}




% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
%\begin{abstract}
%The abstract goes here.
%\end{abstract}

% no keywords




% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}

Since the publication of the first working draft of the human genome in 2001, the National Human Genome Research Institute (NIH) has been tracking the cost of sequencing a human sized genome each year, see Figure~\ref{fig:cost_per_genome}.
Not only has the cost of sequencing a human genome generally decreased each year since 2001, starting in January of 2008 the rate at which the cost was decreasing surpassed Moore's Law\cite{moore1965cramming}.
Subsequently, the number of genomes sequenced per year has been growing at an exponential rate \cite{pagani2012genomes}.

\begin{figure}
    \centering
    \includegraphics[scale=0.3]{img/cost_per_genome_march2016.jpg}
    \caption{The cost of sequencing a human sized genome per year according to the NIH\cite{nih2016dna}.}
    \label{fig:cost_per_genome}
\end{figure}

Although the cost of sequencing technology has drastically declined in the past few years, the common approach to genomics is still ``reference-centric'', that is, a single (reference) genome is used as a representative for a particular species.
Since a reference genome is the sequence of a single individual or a mosaic of individuals as a single linear sequence, this approach is extremely biased.
It is a relic of the foundational period of genomics when sequencing multiple genomes of a particular species was limited by technological and budgetary concerns.
Only quite recently has genomics begun to expand from a single reference per species paradigm into a more comprehensive pan-genome approach that analyzes multiple individuals together.

In this work we will discuss our experience investigating how persistent homology can be used for pan-genome analysis.
To do so, we first provide a background on terminology, algorithms, and data structures for pan-genomics.

\section{Preliminaries}

De Bruijn graphs represent overlap information in a set of DNA sequences \cite{idury1995new, pevzner2001eulerian}.
The vertices in a de Bruijn graph represent words of length $k$ ($k$-mers).
If two $k$-mers overlap by $k-1$ characters then their vertices are connected by a directed edge, the orientation of which reflects the $k$-mers' ordering in the original sequence.
A \emph{bubble} in a de Bruijn graph is a pair of paths that have the same start and end vertices but are otherwise vertex-disjoint.
In \cite{iqbal2012novo}, the authors ``color'' the path of each genome through the de Bruijn graph; see Figure~\ref{fig:coloredDeBruijnExample}.
This allows certain bubbles in the graph to be classified as interesting based on the number of paths they are traversed by and how they are traversed.

\begin{figure}
    \centering
    \includegraphics[scale=0.25]{img/coloredDeBruijnExample.png}
    \caption{This image from \cite{minkin2013sibelia} depicts the $k=3$ de Bruijn graph for two genomes: red (ATCGGTTAACT) and blue (ATCGATCAACT). Each node represents a length $k$ word, and adjacent nodes overlap by $k-1$ characters. Each genome is given its own path through the graph. When the paths are the same, they traverse the same nodes. When they are different, they diverge, forming a bubble.}
    \label{fig:coloredDeBruijnExample}
\end{figure}

A synteny block is a sequence of genes/nucleotides that is conserved within a single genome or related genomes.
Unlike repeats, instances of a synteny block may vary within or between genomes.
Identifying synteny blocks and their variations is integral to inferring phylogenies and functional variation within a group of related species.

Variation in a genome refers to deviation from another sequence, typically a reference.
The types of variation are single nucleotide polymorphisms (SNPs), the insertion or deletion of contiguous sequence (indels), novel repeats, and inversions - the reversal of a repeat.
These variations all form bubbles in a de Bruijn graph; see Figure~\ref{fig:typesOfBubbles}.

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/typesOfBubbles.png}
    \caption{This image from \cite{iqbal2012novo} depicts some of the different types of variation bubbles in a de Bruijn can represent. In \textbf{a}, the red path is a reference genome and the organisms are diploid (an organism with two sets of chromosomes). The heterozygous bubble means two chromosomes of the blue genome have different alleles for a gene but one is the same as the reference. The homozygous bubble means both chromosomes of the blue genome have the same allele but it is different from the reference. The repeat is present in both the reference and the blue genome. In \textbf{b}, there is no reference genome. The repeat is present in all the genomes. The polymorphic site shows deviation from a strongly conserved region.}
    \label{fig:typesOfBubbles}
\end{figure}

\section{Data Set}

We used E-SplitMEM \cite{beller2015efficient} to construct pan-genomes for analysis.
Toy data sets were used during algorithmic development.
To show the relevance and utility of our methods, we constructed the pan-genome for 50 yeast genomes acquired from the Saccharomyces Genome Database [\url{http://www.yeastgenome.org/}].
We chose yeast because it is a well studied genome, enabling us to better validate our results.

\section{Topological Techniques}
\label{sec:techniques}

In our Initial Investigation we investigated four topological techniques: persistent homology, topological density filtration, Fr\'{e}chet Distance, and ridge finding.
We would have liked to pursue all of these techniques, but the amount of work this would have required is well outside the scope of a semester project.
For this reason, we chose to pursue persistent homology and Fr\'{e}chet Distance.

\subsection{Persistent Homology}

Persistent homology is a method for computing topological features of a space at different spatial resolutions.
More persistent features are detected over a wide range of resolutions and so are deemed more likely to represent true features of the underlying space\cite{edelsbrunner2008persistent}.

In genomics, de Bruijn graphs are traditionally used for genome assembly \cite{zerbino2008velvet}.
The larger the $k$ value, the more likely two sequences are to actually overlap in the genome.
For a pan-genome, the function of the $k$ value used to construct the graph is less significant.
Specifically, the $k$ value used dictates the size of the genomic features represented by the graph, and nothing else.
Since it is ambiguous which $k$ value(s) will yield the most significant features, generating de Bruijn graphs for multiple $k$ values and mapping bubbles between them will give a more  comprehensive understanding of the pan-genome.
Enter persistent homology.

For a pan-genome, we can define the spatial resolutions as the $k$ value used to construct the graph and the persistent features as bubbles.
The analysis could be performed as follows:
Generate the colored de Bruijn graph for some initial value $k$.
Then, identify interesting bubbles and track their persistence as $k$ increases.

Not only will this help biologists identify interesting parts of the graph, but how the bubbles persist will help them better understand the evolutionary nature of the graph as well.
For example, a long persisting bubble representing a polymorphic site may represent a mutation that gave the organism an evolutionary advantage. 
In the case of a bubble representing a repeat, how long the bubble persists, the rate at which either side of the bubble degrades, and which paths traversing the bubble persist the longest can all be utilized when inferring phylogenies - a hard problem\cite{felsenstein2004inferring}.

\subsubsection{Identifying Bubbles}

In \cite{iqbal2012novo}, the authors propose two algorithm for identifying bubbles in a de Bruijn graph: bubble calling and path divergence.
We initially implemented both algorithms.
Then, we slightly modified the path divergence algorithm so that all the bubbles identified by the bubble calling algorithm were a subset of the bubbles it identified.
Even so, the algorithm was still a heuristic.
Ultimately, we opted to implement our own exhaustive bubble identification algorithm; see Algorithm~\ref{alg:bubbleMiner}.
Parameter $g$ on Line~\ref{alg:bubbleMiner:procedure} is the de Bruijn graph.
Each path $p$ on Line~\ref{alg:bubbleMiner:loop} is a genome's path through $g$.
The pairs of convergent and divergent points the algorithm finds are the start and end nodes of bubbles.

\begin{algorithm}
    \caption{Identify bubbles}
    \label{alg:bubbleMiner}
    \begin{algorithmic}[1]
        \Procedure{bubbleMiner}{$g$}\label{alg:bubbleMiner:procedure}
            \For{each path $p$ in $g$}\label{alg:bubbleMiner:loop}
                \State // Traverse $p$ and identify where other paths converge with and diverge from $p$
                \State // Sort each path's convergent and divergent points by the order they appear on it
                \State // Pair each divergent point with the first convergent point after it
                \State // Sort each path's convergent and divergent points by the order they appear on $p$
                \State // Pair each divergent point with the first convergent point after it
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

\subsubsection{Computing Bubble Persistence}

For this project, we used E-SplitMEM to construct de Bruijn graphs.
This, and other de Bruijn graph construction algorithms \cite{marcus2014splitmem, minkin2016twopaco}, takes $O(n\log n)$ time to construct a pan-genome.
Although this is efficient enough to construct the pan-genome for a fairly large population of mammalian genomes, it is computationally impractical to construct a pan-genome for each $k$ value you want to consider when computing persistence - there may be thousands!
And so we must emulate the construction of multiple de Bruijn graphs.

In order to emulate how bubbles change between de Bruijn graphs of successive $k$ values, we must analyze how they behave in the actual graphs.
The first key observation that we made is that for increasing values of $k$, the bubbles formed by a pair of genomes will merge to form larger bubbles.
This means the time (how many iterations of the $k$ value) a bubble persists is the genomic distance between the bubble and the next closest bubble on either of the genomes that define it.
The second key observation is that not all bubbles formed by a pair of genomes are in the same order on the genomes, and some may overlap; see Figure~\ref{fig:bubbles}.
This lead us to the following conjecture:

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{img/linearBubbles.png}
        \caption{In this image the bubbles have the same ordering on both genomes.}
        \label{fig:bubbles:linear}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{img/nonLinearBubbles.png}
        \caption{In this image the bubbles do not have the same ordering on both genomes and some are overlapping.}
        \label{fig:bubbles:nonLinear}
    \end{subfigure}
    \caption{These images created by Dr. Brittany Fasy depict bubbles formed by two genomes: red and blue.}\label{fig:bubbles}
\end{figure}

\begin{conj}
Given a set of bubbles formed by two genomes, any two bubbles that are overlapping on one genome will not be overlapping on the other.
\end{conj}

\begin{proof}
By definition, each genome's path through the de Bruijn graph is a contiguous ordering of nodes in the graph.
We say two bubbles are overlapping on a genome's path if the start or end node of one bubble is between the start and end nodes (inclusive) of the other.
Given two bubbles defined by a pair of genomes, we observe that unless the bubbles are not overlapping on at least one of the genomes' paths, then it is impossible for both bubbles to exist.
This is because if the bubbles are overlapping on both genomes' paths, then the only way they can exist in the graph is if at least on of the genomes' paths is not contiguous.
\end{proof}

This same argument holds if we define each path as the nucleotide sequences of the genomes themselves.

Using these observations, we devised an algorithm for computing the persistence of bubbles defined by two genomes; see Algorithm~\ref{alg:bubblePersistence}.
The parameters on Line~\ref{alg:bubblePersistence:procedure} are two lists, both containing the bubbles defined by the two genomes' paths: $p1$ and $p2$.
The bubbles in each genome's list are ordered by their genomic start position on that genome.
The algorithm begins by identifying all the bubbles that are overlapping on one of the genomes' paths and creates a true bubble; Line~\ref{alg:bubblePersistence:initalBubbles}.
A \emph{true bubble} is a bubble that is actually present in the graph.
When a true bubble is created (born), it is given a unique id and all the bubbles that define it are given its id.
Line~\ref{alg:bubblePersistence:initalBubbles} also creates a true bubble for each bubble that is not overlapping with any other bubbles.
Note, every bubble in each genome's list is now part of a true bubble, and so each bubble is marked with the true bubble it is in: $true\_b$.

On Lines~\ref{alg:bubblePersistence:p1dsts} and \ref{alg:bubblePersistence:p2dsts}, the distance between neighboring bubbles on each genome's path is computed and a tuple $(length, b1, b2)$ is added to a new list, where $length$ is the genomic distance between the bubbles, $b1$ is the first bubble on the genome's path, and $b2$ is the second bubble on the genome's path.
On Line~\ref{alg:bubblePersistence:mergeSort}, the two lists are merged into one - $dsts$ - and sorted by $length$.
The sorted list is then iterated from smallest to largest distance on Line~\ref{alg:bublePersistence:iterate}.
For each distance $d$ iterated, it is checked if its bubbles $b1$ and $b2$ are part of the same true bubble; Line~\ref{alg:bubblePersistence:relatedCheck}.
If they are, then their merging does not affect the lifespan of any true bubbles.
If they are not, then their respective true bubbles are merging, meaning both true bubbles will die and a new true bubble is born; Line~\ref{alg:bubblePersistence:life}.
Subsequently, all bubbles that were in the two true bubbles that died are now in the true bubble that was born; Line~\ref{alg:bubblePersistence:orphans}.

\begin{algorithm}
    \caption{Compute bubble persistence}
    \label{alg:bubblePersistence}
    \begin{algorithmic}[1]
        \Procedure{bubblePersistence}{$p1\_bubbles$, $p2\_bubbles$}\label{alg:bubblePersistence:procedure}
            \State // identify initial true bubbles\label{alg:bubblePersistence:initalBubbles}
            \State $p1_dsts\leftarrow neighborDistances(p1\_bubbles)$\label{alg:bubblePersistence:p1dsts}
            \State $p2_dsts\leftarrow neighborDistances(p2\_bubbles)$\label{alg:bubblePersistence:p2dsts}
            \State // merge $t1\_dsts$ and $t2\_dsts$ into a single, sorted list: $dsts$\label{alg:bubblePersistence:mergeSort}
            \For{$d$ in $dsts$}\label{alg:bubblePersistence:iterate}
                \If{$d.b1.true\_b != d.b2.true\_b$}\label{alg:bubblePersistence:relatedCheck}
                    \State // at time $d.length$, $d.b1.true\_b$ and $d.b2.true\_b$ die and a new true bubble is born: $b$\label{alg:trueBubbles:life}
                    \State // all the bubbles that were in $d.b1.true\_b$ or $d.b2.true\_b$ are now in $b$\label{alg:trueBubbles:orphans}
                \EndIf
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Unfortunately, there is a shortcoming to this algorithm - the ``persistence'' being computed is not stable.
This is because if a new bubble is added to the de Bruijn (i.e. a single nucleotide polymorphism), it may drastically affect the persistence of other, more significant bubbles.
Furthermore, since a new true bubble is born every time two die, the persistence is unpredictable.
We've investigated a variety of ways to deal with this problem, including kernel/image homology \cite{cohen2009persistent}.
At the moment, we are looking into hierarchical clustering.

\subsubsection{Hierarchical Clustering}

In hierarchical clustering, each observation starts in its own cluster, and pairs of clusters are merged as the hierarchy is traversed \cite{maimon2005data}; see Figure~\ref{fig:hierarchical}.
This is essentially the approach we have taken so far, where our true bubbles are actually clusters.
In fact, we spent much of the beginning of the semester designing and implementing an inverted tree data structure like the one depicted in Figure~\ref{fig:hierarchical} before realizing it is not actually necessary for computing the persistence of bubbles!
Anyways, what we would like to do is perhaps create some kind of hierarchy with a notation of persistence, such as the that depicted in Figure~\ref{fig:hierarchicalPersistence}.
How we would go about this, we do not know, but it is the current direction we are looking.

\begin{figure}
    \centering
    \includegraphics[scale=0.1]{img/hierarchical_clustering.png}
    \caption{An image from \url{https://en.wikipedia.org/wiki/Hierarchical_clustering} depicting hierarchical clustering. At the beginning, each point is in its own cluster (the leaves). Then they are merged into larger clusters, based on some metric.}
    \label{fig:hierarchical}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.1]{img/hierarchical_clustering2.png}
    \caption{An image from \url{https://en.wikipedia.org/wiki/Hierarchical_clustering} annotated to depict persistent hierarchical clustering, where clusters can persist for multiple levels of the hierarchy and their persistence is determined using some metric, i.e. eldest lives, size, etc.}
    \label{fig:hierarchicalPersistence}
\end{figure}

\subsection{Fr\'{e}chet Distance}

The Fr\'{e}chet distance is a measure of similarity between curves that takes into account the location and ordering of the points along the curves\cite{frechet1906quelques}.
For a pan-genome, the curves are the paths through the graph and the points along the curves are the vertices the paths traverse.
Using the Fr\'{e}chet distance, we hope to compute the variance of the distance between a pair or set of paths.
This would allow researchers to compare the similarity of genomes at a glance.
We would also like to compute a path through the graph that is closest to all the paths through graph, thus creating a ``centroid'' genome.

A centroid genome is interesting from a biological perspective because it is a single linear sequence that is the most representative of the graph.
Essentially, it is a non-biased reference genome for the population.
For this reason, it could be used by traditional analyses that require a reference genome.

Since the curves (paths) are defined as orderings of vertices, and so are not continuous, we must use the discrete Fr\'{e}chet distance\cite{agarwal2012computing, eiter1994computing}.
We also must use a distance metric to define a distance between any two points from a pair of curves.
We chose the Hamming distance \cite{hamming1950error} as the distance metric because it carries more biological meaning than other distance measures and can be computed efficiently.

Instead of reinventing the wheel, we found a Python library that computes discrete Fr\'{e}chet distance, and allows the user to define their own distance measure [\url{https://www.snip2code.com/Snippet/76076/Fr-chet-Distance-in-Python}].
We got the library running on a test data set with our own implementation of the hamming distance metric.
Unfortunately, that is the only progress we have made.
We did devise a method for computing a centroid path through a de Bruijnd graph from all the pairwise Fr\'{e}chet distance variance graphs for the paths in the graph, but no progress was made in proving the validity or this method or implementing it.

\section{Conclusion}

Since the beginning of this project, a paper has been published that discusses the current state of pan-genomics, or rather, what the needs of this new sub-field of genetics are \cite{marschall2016computational}.
Both the persistent homology (hierarchical clustering) and Fr\'{e}chet Distance methods, as well as the other methods we proposed at the beginning of the semester, address many of the open problems in the field.
If the fact that these problems are interesting to work on was not merit enough to pursue these methods, there is now merit galore.
We have made substantial progress this semester and are confident that continuing to work on these methods of pan-genome analysis will yield interesting results.


% use section* for acknowledgment
\section*{Acknowledgment}

We would like to acknowledge Dr. Brittany Fasy for all of her enthusiastic help with this project.
Who knows how far we would've gotten otherwise.





% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{IEEEabrv,biblio}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
%\begin{thebibliography}{1}
%
%\bibitem{IEEEhowto:kopka}
%H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
%  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.
%
%\end{thebibliography}




% that's all folks
\end{document}


