\documentclass[conference]{IEEEtran}

\usepackage{hyperref}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{You Decide 2}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations

\author{\IEEEauthorblockN{Alan Cleary\IEEEauthorrefmark{1},
Eric DiGiovine\IEEEauthorrefmark{2},
Daniel Salinas\IEEEauthorrefmark{3} and
Killian Smith\IEEEauthorrefmark{4}}
\IEEEauthorblockA{Department of Computer Science,\\
Montana State University\\
Bozeman, MT 59717\\
Email: \IEEEauthorrefmark{1}http://alancleary.github.io/\#contact,
\IEEEauthorrefmark{2}edigiovine75@gmail.com,
\IEEEauthorrefmark{3}daniel.salinas@msu.montana.edu,\\
\IEEEauthorrefmark{4}vikingsheepman@gmail.com}}

% conference papers do not typically use \thanks and this command
% is locked out in conference mode. If really needed, such as for
% the acknowledgment of grants, issue a \IEEEoverridecommandlockouts
% after \documentclass

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
%\author{\IEEEauthorblockN{Michael Shell\IEEEauthorrefmark{1},
%Homer Simpson\IEEEauthorrefmark{2},
%James Kirk\IEEEauthorrefmark{3}, 
%Montgomery Scott\IEEEauthorrefmark{3} and
%Eldon Tyrell\IEEEauthorrefmark{4}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}School of Electrical and Computer Engineering\\
%Georgia Institute of Technology,
%Atlanta, Georgia 30332--0250\\ Email: see http://www.michaelshell.org/contact.html}
%\IEEEauthorblockA{\IEEEauthorrefmark{2}Twentieth Century Fox, Springfield, USA\\
%Email: homer@thesimpsons.com}
%\IEEEauthorblockA{\IEEEauthorrefmark{3}Starfleet Academy, San Francisco, California 96678-2391\\
%Telephone: (800) 555--1212, Fax: (888) 555--1212}
%\IEEEauthorblockA{\IEEEauthorrefmark{4}Tyrell Inc., 123 Replicant Street, Los Angeles, California 90210--4321}}




% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
%\begin{abstract}
%The abstract goes here.
%\end{abstract}

% no keywords




% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}

In our Initial Investigation, we investigated how different topological techniques may be used for pan-genome analysis and what data we would evaluate our methods on.
In our first progress report, we chose persistent homology and Fr\'{e}chet Distance as the two techniques we were going to pursue, and we chose a collection of 50 yeast genomes as our data set.
We also reported what progress we had made thus far in applying these techniques to our data.
In this work, we will discuss what progress we have made since the last report.

\section{Data Set}

As discussed in the Initial Investigation and the last report, we have used toy data sets during algorithmic development, each of which was tailored to ensure that the algorithms were behaving as intended.
We have also tested our algorithms on the real data set we expressed interest in, that is, the pan-genome for 50 yeast genomes acquired from the Saccharomyces Genome Database (\url{http://www.yeastgenome.org/}).
We used E-SplitMEM\cite{beller2015efficient} to construct the pan-genomes from raw sequence.
As will be illustrated in Section~\ref{sec:techniques}, we have continued to use the Yeast data set and generated test data sets as needed.

\section{Topological Techniques}
\label{sec:techniques}

In our Initial Investigation we investigated four topological techniques we wanted to pursue: persistent homology, topological density filtration, Fr\'{e}chet Distance, and ridge finding.
As discussed in the last report, we would like to pursue all of these techniques, but came to realize that the amount of work this would require is well outside the scope of a semester project.
For this reason chose to pursue persistent homology and Fr\'{e}chet Distance.
In our last report, we reported the initial progress we made on each of these techniques.
The progress we have made since then is reported below.

\subsection{Persistent Homology}

For a pan-genome (de Bruijn graph), we define the spatial resolution as the $k$ value used to construct the graph and the persistent features as bubbles, where a \emph{bubble} is a pair of genome paths that have the same start and end vertices but are otherwise vertex-disjoint.

\subsubsection{Identifying Bubbles}

We originally implemented both the \emph{bubble calling} and \emph{path divergence} algorithms from \cite{iqbal2012novo} to identify bubbles that we wanted to compute the persistence of.
Later, we realized that the path divergence algorithm can be extended so that the bubbles identified with the bubble calling algorithm are a subset of the bubbles it identifies.
In our previous report, we reported that we had successfully made this extension to the path divergence algorithm.
Since then, we have extensively tested the extended algorithm and have come to the realization that it does not recover all the bubbles in the graph.
For this reason, we cannot guarantee that any persistence diagram we compute will capture all the globally significant features.
Since we want our methods to be as thorough as possible, we have devised and implemented a new algorithm for identifying bubbles that does capture all the bubbles in the graph.

\subsubsection{Bubble Multiplicity}
\label{sec:homology:multiplicity}

A key observation we made when devising how to apply persistent homology is that rather than dying, bubbles merge.
Specifically, all the bubbles on a given genome will eventually merge into a single arc that consists of the entire genome.
So, we want to capture which bubbles merge, when they merge, and how long any one bubble persists between merges.
To do this, we developed an inverted tree data structure.

Each genome in the pan-genome is given its own inverted tree.
The leaves in a tree represent the bubbles that the tree's genome are in.
If a genome is in a bubble multiple times, it has a different leaf for each time it is in the bubble.
We grow the tree by artificially varying $k$.
This is done by computing the distance between each bubble in the tree.
When we identify the bubbles that will merge next we add an internal node that becomes the parent of the merging bubbles.
This process is continued until all the bubbles in a tree have merged into a single node.

In our previous report, we reported that we were debugging and testing the tree construction algorithm.
Our tests revealed that the algorithm was outrageously inefficient.
For this reason, we have reimplemented the algorithm, reducing the running time on the Yeast data set from several hours to a few minutes.

Based on further testing, we have come to another realization: the bubbles a genome participates in may overlap.
We have not completely addressed this issue yet, but are in the process of refactoring the code that computes when bubbles will merge to handle this case.

\subsubsection{Bubble Persistence}

We mentioned in Section~\ref{sec:homology:multiplicity} that we wanted to capture which bubbles merge, when they merge, and how long any one bubble persists between merges.
Since our last report, we have discussed the biological significance of these metrics and have concluded that, although these are the metrics we want to capture, our method of emulating different $k$ values only computes these metrics relative to individual genomes, rather than on a per bubble basis.
%Since our last report, we have discuss the biological significance of these metrics and have concluded that, although these are the metrics we want to capture, rather than report them verbatim, we want to mine more meaningful data from them, specifically, super-bubbles.
This is because although any two genomes in the pan-genome may share a set of bubbles, those bubbles may have different orderings on each genome.
Furthermore, the resolution at which those bubbles merge with other bubbles and with whom they merge may also differ.
%This means when a bubble that is present in two genomes is merged with another bubble in one of the genomes, unless the same merge event occurred in the other genome, then the aforementioned metrics for each genome can only be used to make inferences about that genome.
In other words, the persistence of the bubbles that we are computing is local to each genome, rather than global in the entire de Bruijn graph.
As the purpose of constructing and analyzing a pan-genome is to capture and characterize variation within a population, this is a problem.

%Our solution to the problem is super-bubbles.
To go from local persistence to global, we propose to use the multiplicity of the bubbles relative to each genome, as captured by the inverted bubble trees, to characterize the birth and death of global bubbles.
Specifically, we have observed that as the $k$ value of the de Bruijn graph increases, a merging of bubbles on one genome can span an entire subset of bubbles on another genome, making the subset ordered relative to both genomes, thus forming a new bubble.
This will be considered the birth of a global bubble.
When two global bubbles merge, both will die and a new global bubble will be born.

It is still unclear how these global bubbles will be computed.
Since either arc of a global bubble can be a chain of local bubbles that have not yet merged, and either arc can consume an arbitrary number of local bubbles before merging with another global bubble, the bubble may span multiple levels of different inverted bubble trees.
So, one of our goals for the next progress report is to work out how to do this and generate some persistence diagrams.
Speaking of which, since our last report, we have implemented code that will draw barcode diagrams.

%Looking forward, we are contemplating how to leverage our forest of inverted bubble trees in a biologically meaningful way.
%We believe how the bubbles persist will help geneticists better understand the evolutionary nature of the population the graph represents.
%For example, a long persisting bubble representing a polymorphic site would be more evolutionarily significant than a short persisting one. 
%In the case of a bubble representing a repeat, how long the bubble persists, the rate at which either side of the bubble degrades, and which paths traversing the bubble persist the longest can all be utilized when inferring phylogenies - a hard problem\cite{felsenstein2004inferring}.
%We will consult with collaborators (domain experts) at the National Center for Genome Resources (NCGR) during this phase of the project to make sure our methods are sound.

\subsection{Fr\'{e}chet Distance}

The curves we are measuring the similarity between are the genome paths through the de Bruijn graph.
Since the curves are defined as orderings of vertices, and so are not continuous, we must use the discrete Fr\'{e}chet distance\cite{agarwal2012computing, eiter1994computing}.
In our last report, we reported that we chose the Hamming distance\cite{hamming1950error} as the distance measure between any two nodes in the graph because it carries more biological meaning than other measures and can be computed efficiently.
Since then, we have been searching for an off-the-shelf Fr\'{e}chet distance implementation that allows us to use the Hamming distance measure and can easily integrate into our existing codebase.
We ended up settling on a Python library that allows the user to define their own distance measure, a surprisingly rare feature.
We have gotten the library running on its own and verified that it works with the hamming distance.
The next step, which we hope to achieve before the next progress report, is to get the library running on our graph data structure, ideally one of our test pan-genomes.


\section{Conclusion}

Due to the setbacks mentioned, we are not as far along as we anticipated we would be by this report.
Still, we have done much work and confronted many interesting problems since the last report.
We are optimistic that we will be able to generate some persistent homology results by the end of the semester that we can send to our collaborators at the National Center for Genome Resources for analysis.
As far as the Fr\'{e}chet distance is concerned, it seems as though we will only be able to achieve preliminary results by the end of the semester, i.e. what the variance of the distance between any two genomes in the graph is.
Since we are using the Hamming distance, this will still be biologically interesting and will likely guide further investigation.


% use section* for acknowledgment
\section*{Acknowledgment}

We would like to acknowledge Foghorn Leghorn, the exemplar rooster.





% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{IEEEabrv,biblio}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
%\begin{thebibliography}{1}
%
%\bibitem{IEEEhowto:kopka}
%H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
%  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.
%
%\end{thebibliography}




% that's all folks
\end{document}


