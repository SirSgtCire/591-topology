\documentclass[conference]{IEEEtran}

\usepackage{hyperref}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Initial Investigation}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations

\author{\IEEEauthorblockN{Alan Cleary\IEEEauthorrefmark{1},
Eric DiGiovine\IEEEauthorrefmark{2},
Daniel Salinas\IEEEauthorrefmark{3} and
Killian Smith\IEEEauthorrefmark{4}}
\IEEEauthorblockA{Department of Computer Science,\\
Montana State University\\
Bozeman, MT 59717\\
Email: \IEEEauthorrefmark{1}http://alancleary.github.io/\#contact,
\IEEEauthorrefmark{2}edigiovine75@gmail.com,
\IEEEauthorrefmark{3}daniel.salinas@msu.montana.edu,\\
\IEEEauthorrefmark{4}vikingsheepman@gmail.com}}

% conference papers do not typically use \thanks and this command
% is locked out in conference mode. If really needed, such as for
% the acknowledgment of grants, issue a \IEEEoverridecommandlockouts
% after \documentclass

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
%\author{\IEEEauthorblockN{Michael Shell\IEEEauthorrefmark{1},
%Homer Simpson\IEEEauthorrefmark{2},
%James Kirk\IEEEauthorrefmark{3}, 
%Montgomery Scott\IEEEauthorrefmark{3} and
%Eldon Tyrell\IEEEauthorrefmark{4}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}School of Electrical and Computer Engineering\\
%Georgia Institute of Technology,
%Atlanta, Georgia 30332--0250\\ Email: see http://www.michaelshell.org/contact.html}
%\IEEEauthorblockA{\IEEEauthorrefmark{2}Twentieth Century Fox, Springfield, USA\\
%Email: homer@thesimpsons.com}
%\IEEEauthorblockA{\IEEEauthorrefmark{3}Starfleet Academy, San Francisco, California 96678-2391\\
%Telephone: (800) 555--1212, Fax: (888) 555--1212}
%\IEEEauthorblockA{\IEEEauthorrefmark{4}Tyrell Inc., 123 Replicant Street, Los Angeles, California 90210--4321}}




% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
%\begin{abstract}
%The abstract goes here.
%\end{abstract}

% no keywords




% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}

Since the publication of the first working draft of the human genome in 2001, the National Human Genome Research Institute (NIH) has been tracking the cost of sequencing a human sized genome each year, see Figure~\ref{fig:cost_per_genome}.
Not only has the cost of sequencing a human genome generally decreased each year since 2001, starting in January of 2008 the rate at which the cost was decreasing surpassed Moore's Law\cite{moore1965cramming}.
Subsequently, the number of genomes sequenced per year has been growing at an exponential rate \cite{pagani2012genomes}.

Although the cost of sequencing technology has drastically declined in the past few years, the common approach to genomics is still ``reference-centric'', that is, a single (reference) genome is used as a representative for a particular species.
Since a reference genome is the sequence of a single individual or a mosaic of individuals as a single linear sequence, this approach is extremely biased.
It is a relic of the foundational period of genomics when sequencing multiple genomes of a particular species was limited by technological and budgetary concerns.
Only quite recently has genomics begun to expand from a single reference per species paradigm into a more comprehensive pan-genome approach that analyzes multiple individuals together.

In this work we investigate how different topological techniques may be used for pan-genome analysis.
To do so, we first provide a background on terminology, algorithms, and data structures for pan-genomics.

%This paper explores recent algorithms and data structures in the pan-genomics space.
%It is a critical evaluation of the current state of pan-genomics from a computational perspective.
%Furthermore, it summarizes the shortcomings and needs of this new paradigm as exemplified by the literature discussed and proposes new research directions.

\section{Pan-Genomics}
\label{sec:pan}
% what is it?
The term ``pan-genomics'' was first coined by Tettelin et al in \cite{tettelin2005genome}.
Given multiple genomes of the same or closely related species, the ``pan-genome'' is a single comprehensive catalog of all the sequences and variants in the population.
It is typically composed of three parts: the core genome, the dispensable genome, and the strain-specific genome.
The core genome contains the genes that are shared by all the genomes within the population.
These genes are typically associated with the basic biology of the population and major phenotypic traits.
The dispensable genome contains genes that are shared by a subset of the genomes in the population.
These genes contribute to the diversity of the population, such as niche specific adaptations.
The strain-specific genome contains genes that are present within a single genome in the population.

Today there exists a variety of tools for pan-genomic analysis\cite{vernikos2015ten}.
Until quite recently the algorithms and data structures of these tools have been analysis specific.
Furthermore, many of these tools rely on gene annotation data rather than raw sequence.
This means they are susceptible to inconsistencies between annotation sources, the overall quality of annotation, and are incapable of analyzing genomes that are not annotated.

In an attempt to standardize pan-genome analysis while avoiding the pitfalls of annotation data, three recent tools construct pan-genomes from raw sequence data and output the pan-genome as a colored de Bruijn graph for further analysis.
These tools are \emph{Sibelia}\cite{minkin2013sibelia}, \emph{SplitMEM}\cite{marcus2014splitmem}, and what we will call \emph{E-SplitMEM}\cite{beller2015efficient}.
Unfortunately, these tools are primarily concerned with the representation of pan-genomes, rather than their analysis.
For this reason we are concerned with the analysis of the pan-genomes generated by these tools.
%In the following sections these tools will be discussed in detail.
%But first, the \emph{Cortex}\cite{iqbal2012novo} assembler will be discussed to better understand the colored de Bruijn graph data structure, which is used by all these tools.
%Lastly, in recognition of the lack of scalability of these tools, we will discuss \emph{RACE}\cite{mansour2013race}, a tool that could be extended to construct pan-genomes in a parallel computing environment.

\begin{figure}
    \centering
    \includegraphics[scale=0.3]{img/costpergenome2015_4.jpg}
    \caption{The cost of sequencing a human sized genome per year according to the NIH\cite{nih2016dna}.}
    \label{fig:cost_per_genome}
\end{figure}

\section{Preliminaries}

De Bruijn graphs represent overlap information in a set of DNA sequences \cite{idury1995new, pevzner2001eulerian}.
The vertices in a de Bruijn graph represent words of length $k$ ($k$-mers).
If two $k$-mers overlap by $k-1$ characters then their vertices are connected by a directed edge, the orientation of which reflects the $k$-mers' ordering in the original sequence.
A \emph{bubble} in a de Bruijn graph is a pair of paths that have the same start and end vertices but are otherwise vertex-disjoint.
In \cite{iqbal2012novo}, the authors ``color'' the path of each genome through the de Bruijn graph.
This allows certain bubbles in the graph to be classified as interesting based on the number of paths they are traversed by and how they are traversed.

A synteny block is a sequence of genes/nucleotides that is conserved within a single genome or related genomes.
Unlike repeats, instances of a synteny block may vary within or between genomes.
Identifying synteny blocks and their variations is integral to inferring phylogenies and functional variation within a group of related species.

Variation in a genome refers to deviation from another sequence, typically a reference.
The types of variation are single nucleotide polymorphisms (SNPs), the insertion or deletion of contiguous sequence (indels), novel repeats, and inversions - the reversal of a repeat.
These variations all form bubbles in a de Bruijn graph.

\section{Data Set}

We will use E-SplitMEM to construct pan-genomes for analysis.
Toy data sets will be used during algorithmic development.
Then, to show the relevance and utility of our analyses, we will construct the pan-genome for 50 yeast genomes acquired from the Saccharomyces Genome Database (\url{http://www.yeastgenome.org/}).
We chose yeast because it is a well studied genome, which will enable us to better validate our results.

\section{Topological Techniques}

We would like to pursue each of the following topological techniques, time permitting.

\subsection{Persistent Homology}

Persistent homology is a method for computing topological features of a space at different spatial resolutions.
More persistent features are detected over a wide range of resolutions and so are deemed more likely to represent true features of the underlying space\cite{edelsbrunner2008persistent}.

For a pan-genome, we can define the spatial resolutions as the $k$ value used to construct the graph and the persistent features as bubbles.
The analysis could be performed as follows:
Generate the colored de Bruijn graph for some initial value $k$.
Then, use the bubble calling and path divergence algorithms from \cite{iqbal2012novo} to identify interesting bubbles.
The persistence of these bubbles can then be tracked for increasing values of $k$.

Not only will this help biologists identify interesting parts of the graph, but how the bubbles persist will help them better understand the evolutionary nature of the graph as well.
For example, a long persisting bubble representing a polymorphic site would be more evolutionarily significant than a short persisting one. 
In the case of a bubble representing a repeat, how long the bubble persists, the rate at which either side of the bubble degrades, and which paths traversing the bubble persist the longest can all be utilized when inferring phylogenies - a hard problem\cite{felsenstein2004inferring}.

\subsection{Topological Density Filtration}

In statistics, a density function describes the relative likelihood of a random variable taking on a value.
Given a topological structure, the density estimated by some function can be filtered in a persistent manner, meaning more dense features of the topology will persist longer\cite{fasy2014statistical}.
For a pan-genome, the density function could estimate density from a variety of properties, such as the number of paths that traverse a vertex or edge, how long a bubble persists as $k$ increases, or how many genome annotations map to a particular vertex or edge.

Filtration of these densities on a pan-genome will help to identify biologically interesting portions of the graph.
The filtration could also be used to discover ``hot'' parts of the graph, that is, parts of the graph where many relatively dense features are in close proximity.
This could potentially capture syntenic blocks that a fractured by noise (SNPs, indels, etc).

\subsection{Fr\'{e}chet Distance}

The Fr\'{e}chet distance is a measure of similarity between curves that takes into account the location and ordering of the points along the curves\cite{frechet1906quelques}.
For a pan-genome, the curves are the paths through the graph and the points along the curves are the vertices the paths traverse.
Using the Fr\'{e}chet distance, we hope to compute a path through the graph that is closest to all the paths through graph, thus creating a ``centroid'' genome.

This is interesting from a biological perspective because the centroid is a single linear sequence that is the most representative of the graph.
Essentially, it is a non-biased reference genome for the population.
For this reason, it could be used by traditional analyses that require a reference genome.

\subsection{Ridge Finding}

In topology, ridge finding is similar to density filtration in that the density estimated by some function can be filtered in a persistent manner.
It differs in that as the filtration proceeds it looks for paths of high density that emerge between some start point and some end point.
Since these paths tend to have higher density they form ``ridges'' in the density landscape\cite{chen2014generalized}.

To perform ridge finding on a pan-genome we will need to add a start vertex and an end vertex that are shared by all the paths in the graph.
We can then perform density filtration to find ridges.
Assuming the genomes used to construct the pan-genome were sampled independently and are identically distributed, these ridges would represent genomes that are likely to occur in the population. 
If we wanted to ``bootstrap'' a large set of genomes from a relatively small collection of high density ridges, we could perturb the ridges by introducing random deviations with some probability, perhaps based on other information about the paths in the graph.

Genomes generated in this manner, paired with the centroid genome, could be analyzed with tools designed for the analysis of single genomes.
The results of these tools could then be aggregated and used to make inferences about the population the pan-genome represents.

%\subsection{Bootstrapping Topology}
%
%Topological bootstrapping is the reconstruction of global topological properties of a complex network starting from limited information.
%Often this requires the use of a non-topological quantity that can be interpret as fitness\cite{musmeci2013bootstrapping}.
%Given a pan-genome, we would like to reconstruct paths that are likely to occur in the graph.
%The limited information we would start with would be a collection of nodes acquired via random sampling of the graph and the non-topological quantities would be the paths that traverse the graph.
%
%Genomes generated in this manner, paired with the centroid genome, could be analyzed with tools designed for the analysis of single genomes.
%The results of these tools could then be aggregated and used to make inferences about the population the pan-genome represents.


\section{Conclusion}

Each of these analyses are interesting in their own right, though combining them could be quite useful.
Case in point, when drawing the de Bruijn graph for a moderately sized population of genomes, the output often looks like a hairball, as seen Figure~\ref{fig:eColiPan1}.
This visualization is so dense and complex that no useful information can be gleaned from it.

Graph sketching is the process of identifying interesting regions of a complex graph and using them to construct a compact graph that characterizes the underlying data\cite{ahn2012graph}.
The methods described in this work could be used to identify interesting regions of a pan-genome from which a useful sketch could be created.

\begin{figure}
    \centering
        \includegraphics[scale=0.3]{img/eColiPan1.png}
    \caption{The de Bruijn graph for the E.coli pan-genome with $k=25$ visualized with Gephi from \cite{marcus2014splitmem}.}
    \label{fig:eColiPan1}
\end{figure}

% conference papers do not normally have an appendix


% use section* for acknowledgment
\section*{Acknowledgment}

We would like to acknowledge gummy bears.
Without them we would be lost.





% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{IEEEabrv,biblio}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
%\begin{thebibliography}{1}
%
%\bibitem{IEEEhowto:kopka}
%H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
%  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.
%
%\end{thebibliography}




% that's all folks
\end{document}


