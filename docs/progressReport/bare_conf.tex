\documentclass[conference]{IEEEtran}

\usepackage{hyperref}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

% for writing theorems, definitions, conjectures, etc
\usepackage{amsthm}
\theoremstyle{plain}
\newtheorem{thm}{Theorem} % reset theorem numbering for each chapter
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\theoremstyle{conjecture}
\newtheorem{conj}{Conjecture}

% for writing algorithms
\usepackage{algorithm, algpseudocode}

% correct bad hyphenation here
\hyphenation{}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Progress Report}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations

\author{\IEEEauthorblockN{Alan Cleary\IEEEauthorrefmark{1},
Eric DiGiovine\IEEEauthorrefmark{2},
Daniel Salinas\IEEEauthorrefmark{3} and
Killian Smith\IEEEauthorrefmark{4}}
\IEEEauthorblockA{Department of Computer Science,\\
Montana State University\\
Bozeman, MT 59717\\
Email: \IEEEauthorrefmark{1}http://alancleary.github.io/\#contact,
\IEEEauthorrefmark{2}edigiovine75@gmail.com,
\IEEEauthorrefmark{3}daniel.salinas@msu.montana.edu,\\
\IEEEauthorrefmark{4}vikingsheepman@gmail.com}}

% conference papers do not typically use \thanks and this command
% is locked out in conference mode. If really needed, such as for
% the acknowledgment of grants, issue a \IEEEoverridecommandlockouts
% after \documentclass

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
%\author{\IEEEauthorblockN{Michael Shell\IEEEauthorrefmark{1},
%Homer Simpson\IEEEauthorrefmark{2},
%James Kirk\IEEEauthorrefmark{3}, 
%Montgomery Scott\IEEEauthorrefmark{3} and
%Eldon Tyrell\IEEEauthorrefmark{4}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}School of Electrical and Computer Engineering\\
%Georgia Institute of Technology,
%Atlanta, Georgia 30332--0250\\ Email: see http://www.michaelshell.org/contact.html}
%\IEEEauthorblockA{\IEEEauthorrefmark{2}Twentieth Century Fox, Springfield, USA\\
%Email: homer@thesimpsons.com}
%\IEEEauthorblockA{\IEEEauthorrefmark{3}Starfleet Academy, San Francisco, California 96678-2391\\
%Telephone: (800) 555--1212, Fax: (888) 555--1212}
%\IEEEauthorblockA{\IEEEauthorrefmark{4}Tyrell Inc., 123 Replicant Street, Los Angeles, California 90210--4321}}




% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
%\begin{abstract}
%The abstract goes here.
%\end{abstract}

% no keywords




% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}

In our Initial Investigation, we investigated how different topological techniques may be used for pan-genome analysis and what data we would evaluate our methods on.
In our first progress report, we chose persistent homology and Fr\'{e}chet Distance as the two techniques we were going to pursue, and we chose a collection of 50 yeast genomes as our data set.
We also reported what progress we had made in applying these techniques to our data.
In our second progress report, we reported implementing an exhaustive bubble identification algorithm, refactoring our bubble tree construction algorithm, and discussed the problem of computing which bubbles actually occur in the de Bruijn graph at any given resolution.
In this work, we will discuss what progress we have made since the last report.

\section{Data Set}

As discussed in the Initial Investigation and the last two reports, we have used toy data sets during algorithmic development, each of which was tailored to ensure that our algorithms were behaving as intended.
We have also tested our algorithms on the real data set we expressed interest in, that is, the pan-genome for 50 yeast genomes acquired from the Saccharomyces Genome Database (\url{http://www.yeastgenome.org/}).
We used E-SplitMEM\cite{beller2015efficient} to construct the pan-genomes from raw sequence.
As will be illustrated in Section~\ref{sec:techniques}, we have continued to use the Yeast data set and generated test data sets as needed.

\section{Topological Techniques}
\label{sec:techniques}

In our Initial Investigation we investigated four topological techniques we wanted to pursue: persistent homology, topological density filtration, Fr\'{e}chet Distance, and ridge finding.
As discussed in the last report, we would like to pursue all of these techniques, but came to realize that the amount of work this would require is well outside the scope of a semester project.
For this reason, we chose to pursue persistent homology and Fr\'{e}chet Distance.
In our first two reports, we reported the progress we have made on each of these techniques.
The progress we have made since the last report is reported below.

\subsection{Bubble Trees}
\label{sec:bubbleTrees}
In our last progress report, we discussed how we had designed and implemented an inverted tree data structure for capturing when bubbles merge on any given genome.
This was motivated by the observation that rather than dying, bubbles merge, which implies that all the bubbles on a given genome will eventually merge into a single arc that consists of the entire genome.
By using a tree, we are able to capture which bubbles merge, when they merge, and how long any one bubble persists between merges.

Each genome in the pan-genome is given its own inverted tree.
The leaves in a tree represent the bubbles that the tree's genome is part of.
If a genome is in a bubble multiple times, it has a different leaf for each time it is in the bubble.
We grow the tree by artificially varying $k$.
This is done by ordering the bubbles in the tree by their genomic position on the tree's genome and then computing the distance between each pair of neighboring bubbles.
We then merge bubbles in the order of smallest distance to largest.
To merge two or more bubbles, we add an internal node that becomes the parent of the merging bubbles.
This process is continued until all the bubbles in a tree have merged into a single node.

Since the previous update, we have realized that taking this approach can lead to several of the bubbles overlapping.
This makes the merging of bubbles non-trivial since it becomes ambiguous when bubbles should merge, if ever at all.
Our solution was to make each inverted tree into a collection of disjoint trees that share some of the same leaf nodes.
Specifically, we construct a disjoint tree for each genome that the inverted tree's genome forms bubbles with.
Each such tree is constructed as before.

Although the disjoint trees solve some of the overlaps, they do not solve all of them.
Fortunately, we are able to handle the rest of the overlaps by exploiting the fact that each disjoint tree only represents the bubbles formed by a pair of genomes.
Our method is based on the following conjecture:

\begin{conj}
Given a set of bubbles shared by two genomes, any two bubbles that are overlapping on one genome will be linearly ordered on the other.
\end{conj}

The conjecture assumes that if one bubble begins on a nucleotide that another ends on, the bubbles are separate.
Although we have not considered the case where more than two bubbles are overlapping, we have used the conjecture to assert that any information that is lost due to the overlapping of bubbles on one genome will be preserved by the linear ordering of the bubbles on the other genome.
As such, we have decided to merge overlapping bubbles prior to running the tree construction algorithm.
Specifically, we order the bubbles by their genomic start positions and then identify groups of bubbles that are overlapping.
For each such group, we say the distance between the bubbles is 0 and merge them into a single bubble.
The result is that the bubbles given to the tree construction algorithm are ordered by their position on the tree's genome and are not overlapping.

An interesting side note is how we are computing the genomic start and end positions of the bubbles, which are used when computing the distance between neighboring bubbles on a genome.
The start position should be the genome string index of the last nucleotide the two genomes have in common before they diverge, and the end position should be the genome string index of the first nucleotide the two genomes merge on after the divergence.
Subsequently, we compute the start position as the position of the sequence for the node in the de Bruijn graph the bubble starts on, plus the node's length.
The end position is the position of the sequence for the node in the de Bruijn graph the bubble ends on.

Since our last progress report, we have noticed that in the case of an insertion, the start position of a bubble comes after the end position!
This is because an insertion occurs between sequential nucleotides in a sequence, or rather, adjacent nodes in the de Bruijn graph.
Since adjacent nodes overlap by $k-1$ characters, the start and end positions computed for a bubble formed by an insertion not only say the bubble ends before it starts, but define the genomic interval in which the novel sequence could have been inserted.
Our solution is to find the midpoint between the two positions and use that as the start position and the next nucleotide as the end position.
This solution is a compromise because putting the insertion anywhere in the interval will yield the same quality alignment \cite{needleman1970general, smith1981identification}.
By putting the insertion in the middle, we are not introducing any bias, that is, no bubble is merged sooner than need be.

\subsection{Bubble Persistence}
In our last progress report, we discussed how the bubbles represented by an inverted tree were ``local'' from the perspective of the tree's genome and did not necessarily represent bubbles that were ``global'' in the de Bruijn graph.
Since then, we have learned that local and global are reserved terms in homology, and so we will simply refer to bubbles that a pair of inverted trees agree exist in the de Bruijn graph as \emph{true bubbles}.

Since our last progress report, we have devised an algorithm for identifying true bubbles from a pair of inverted trees; see Algorithm~\ref{alg:trueBubbles}.
The parameters $t1\_dsts$ and $t2\_dsts$ on Line~\ref{alg:trueBubbles:procedure} are lists of distances between neighboring nodes in trees $t1$ and $t2$ computed using the method in Section~\ref{sec:bubbleTrees}.
Each element in the lists is a tuple $(length, b1, b2)$, where $b1$ and $b2$ are the neighboring bubbles the distance was computed between and $length$ is the distance.

The algorithm begins by identifying all the bubbles the pair of trees already agree are true; Line~\ref{alg:trueBubbles:initalTrue}.
Note, every bubble in each tree is part of a true bubble at this phase, and so each bubble is marked with the true bubble it is in: $true\_b$.
Then, on Line~\ref{alg:trueBubbles:mergeSort}, the two lists are merged into one - $dsts$ - and sorted by $length$.
The sorted list is then iterated from smallest to largest distance on Line~\ref{alg:trueBubbles:iterate}.
For each distance $d$ iterated, it is checked if its bubbles $b1$ and $b2$ are part of the same true bubble; Line~\ref{alg:trueBubbles:relatedCheck}.
If they are, then their merging does not affect the lifespan of any true bubbles.
If they are not, then their respective true bubbles are merging, meaning both true bubbles will die and a new true bubble is born; Line~\ref{alg:trueBubbles:life}.
Subsequently, all bubbles that were in the two true bubbles that died are now in the true bubble that was born; Line~\ref{alg:trueBubbles:orphans}.

\begin{algorithm}
    \caption{Compute true bubble persistence}
    \label{alg:trueBubbles}
    \begin{algorithmic}[1]
        \Procedure{findTrueBubbles}{$t1\_dsts$, $t2\_dsts$}\label{alg:trueBubbles:procedure}
            \State // identify initial true bubbles\label{alg:trueBubbles:initalTrue}
            \State // merge $t1\_dsts$ and $t2\_dsts$ into a single, sorted list: $dsts$\label{alg:trueBubbles:mergeSort}
            \For{$d$ in $dsts$}\label{alg:trueBubbles:iterate}
                \If{$d.b1.true\_b != d.b2.true\_b$}\label{alg:trueBubbles:relatedCheck}
                    \State // at time $d.length$, $d.b1.true\_b$ and $d.b2.true\_b$ die and a new true bubble is born: $b$\label{alg:trueBubbles:life}
                    \State // all the bubbles that were in $d.b1.true\_b$ or $d.b2.true\_b$ are now in $b$\label{alg:trueBubbles:orphans}
                \EndIf
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Although Algorithm~\ref{alg:trueBubbles} computes the persistence of true bubbles in the de Bruijn graph, there are a few serious points worth discussing.
First is that the algorithm does not actually require the inverted trees in order to compute the persistence of true bubbles.
As we have mentioned, the distances the algorithm uses are computed when constructing the trees, but the trees need not be constructed in order to compute the distances.
The second and more serious point is that the ``persistence'' computed is not stable.
This is because if a new bubble is added to the de Bruijn (i.e. a single nucleotide polymorphism), it will drastically affect the persistence of other, more significant bubbles.
Furthermore, since a new true bubble is born every time two die, the persistence is unpredictable.
To make the computed persistence stable, we are investigating kernel/image homology \cite{cohen2009persistent}.


%\subsection{Persistent Homology}
%
%For a pan-genome (de Bruijn graph), we define the spatial resolution as the $k$ value used to construct the graph and the persistent features as bubbles, where a \emph{bubble} is a pair of genome paths that have the same start and end vertices but are otherwise vertex-disjoint.
%
%\subsubsection{Identifying Bubbles}
%
%We originally implemented both the \emph{bubble calling} and \emph{path divergence} algorithms from \cite{iqbal2012novo} to identify bubbles that we wanted to compute the persistence of.
%Later, we realized that the path divergence algorithm can be extended so that the bubbles identified with the bubble calling algorithm are a subset of the bubbles it identifies.
%In our previous report, we reported that we had successfully made this extension to the path divergence algorithm.
%Since then, we have extensively tested the extended algorithm and have come to the realization that it does not recover all the bubbles in the graph.
%For this reason, we cannot guarantee that any persistence diagram we compute will capture all the globally significant features.
%Since we want our methods to be as thorough as possible, we have devised and implemented a new algorithm for identifying bubbles that does capture all the bubbles in the graph.
%
%\subsubsection{Bubble Multiplicity}
%\label{sec:homology:multiplicity}
%
%A key observation we made when devising how to apply persistent homology is that rather than dying, bubbles merge.
%Specifically, all the bubbles on a given genome will eventually merge into a single arc that consists of the entire genome.
%So, we want to capture which bubbles merge, when they merge, and how long any one bubble persists between merges.
%To do this, we developed an inverted tree data structure.
%
%Each genome in the pan-genome is given its own inverted tree.
%The leaves in a tree represent the bubbles that the tree's genome are in.
%If a genome is in a bubble multiple times, it has a different leaf for each time it is in the bubble.
%We grow the tree by artificially varying $k$.
%This is done by computing the distance between each bubble in the tree.
%When we identify the bubbles that will merge next we add an internal node that becomes the parent of the merging bubbles.
%This process is continued until all the bubbles in a tree have merged into a single node.
%
%In our previous report, we reported that we were debugging and testing the tree construction algorithm.
%Our tests revealed that the algorithm was outrageously inefficient.
%For this reason, we have reimplemented the algorithm, reducing the running time on the Yeast data set from several hours to a few minutes.
%
%Based on further testing, we have come to another realization: the bubbles a genome participates in may overlap.
%We have not completely addressed this issue yet, but are in the process of refactoring the code that computes when bubbles will merge to handle this case.
%
%\subsubsection{Bubble Persistence}
%
%We mentioned in Section~\ref{sec:homology:multiplicity} that we wanted to capture which bubbles merge, when they merge, and how long any one bubble persists between merges.
%Since our last report, we have discussed the biological significance of these metrics and have concluded that, although these are the metrics we want to capture, our method of emulating different $k$ values only computes these metrics relative to individual genomes, rather than on a per bubble basis.
%%Since our last report, we have discuss the biological significance of these metrics and have concluded that, although these are the metrics we want to capture, rather than report them verbatim, we want to mine more meaningful data from them, specifically, super-bubbles.
%This is because although any two genomes in the pan-genome may share a set of bubbles, those bubbles may have different orderings on each genome.
%Furthermore, the resolution at which those bubbles merge with other bubbles and with whom they merge may also differ.
%%This means when a bubble that is present in two genomes is merged with another bubble in one of the genomes, unless the same merge event occurred in the other genome, then the aforementioned metrics for each genome can only be used to make inferences about that genome.
%In other words, the persistence of the bubbles that we are computing is local to each genome, rather than global in the entire de Bruijn graph.
%As the purpose of constructing and analyzing a pan-genome is to capture and characterize variation within a population, this is a problem.
%
%%Our solution to the problem is super-bubbles.
%To go from local persistence to global, we propose to use the multiplicity of the bubbles relative to each genome, as captured by the inverted bubble trees, to characterize the birth and death of global bubbles.
%Specifically, we have observed that as the $k$ value of the de Bruijn graph increases, a merging of bubbles on one genome can span an entire subset of bubbles on another genome, making the subset ordered relative to both genomes, thus forming a new bubble.
%This will be considered the birth of a global bubble.
%When two global bubbles merge, both will die and a new global bubble will be born.
%
%It is still unclear how these global bubbles will be computed.
%Since either arc of a global bubble can be a chain of local bubbles that have not yet merged, and either arc can consume an arbitrary number of local bubbles before merging with another global bubble, the bubble may span multiple levels of different inverted bubble trees.
%So, one of our goals for the next progress report is to work out how to do this and generate some persistence diagrams.
%Speaking of which, since our last report, we have implemented code that will draw barcode diagrams.
%
%%Looking forward, we are contemplating how to leverage our forest of inverted bubble trees in a biologically meaningful way.
%%We believe how the bubbles persist will help geneticists better understand the evolutionary nature of the population the graph represents.
%%For example, a long persisting bubble representing a polymorphic site would be more evolutionarily significant than a short persisting one. 
%%In the case of a bubble representing a repeat, how long the bubble persists, the rate at which either side of the bubble degrades, and which paths traversing the bubble persist the longest can all be utilized when inferring phylogenies - a hard problem\cite{felsenstein2004inferring}.
%%We will consult with collaborators (domain experts) at the National Center for Genome Resources (NCGR) during this phase of the project to make sure our methods are sound.
%
\subsection{Fr\'{e}chet Distance}
The curves we are measuring the similarity between are the genome paths through the de Bruijn graph.
Since the curves are defined as orderings of vertices, and so are not continuous, we must use the discrete Fr\'{e}chet distance\cite{agarwal2012computing, eiter1994computing}.
In our first progress report, we reported that we chose the Hamming distance\cite{hamming1950error} as the distance measure between any two nodes in the graph because it carries more biological meaning than other distance measures and can be computed efficiently.
%Since then, we have been searching for an off-the-shelf Fr\'{e}chet distance implementation that allows us to use the Hamming distance measure and can easily integrate into our existing codebase.
In our last progress report, we announced that we had found a Python library for computing discrete Fr\'{e}chet distance that allowed us to define our own distance measure, which we confirmed by running the library on a test data set with the hamming distance.
In that report, we said our next goal was to get the library running on our graph data structure, ideally one of our test pan-genomes.
Unfortunately, we have made no progress since then.
This is due to us focussing our efforts on the persistent homology component of the project.
Although it would be nice to make progress on the Fr\'{e}chet distance work, we will give the persistent homology work priority.


\section{Presentation Feedback}
On Thursday, March 24 we gave a presentation to our peers about our project.
Later, we submitted a summary of their responses to our presentation.
As none of their comments or feedback pertained to the technical or theoretical aspects of our project, we have not been able to leverage their responses.

Our final presentation is on Tuesday, April 26.
We will be sure to consider our peers' responses when preparing for this presentation.
Specifically, we will be sure the pace at which we speak is not too fast, that we do not go over time, and that all of our images will be of the utmost quality.


\section{Conclusion}

We have made substantial progress since our last progress report: we have formulated and implemented a new tree construction method, we have solved the problem of overlapping bubbles based on a conjecture we have made, we have found a nice solution to the problem of computing the start and end position of bubbles that represent insertions, and we have devised an algorithm for identifying true bubbles and computing their persistence.
Looking forward, we would like to resolve the issue of our persistence diagrams not being stable and prove our conjecture.
We would also like to get some preliminary results for the yeast data, which will guide further revisions of our methods.
Last, but not least, we would like to make additional progress on the Fr\'{e}chet distance work.
Although we may only be able to achieve preliminary results by the end of the semester (i.e. what the variance of the distance between any two genomes in the graph is) we believe this will still be biologically interesting and will likely guide further investigation.


% use section* for acknowledgment
\section*{Acknowledgment}

We would like to acknowledge pie.





% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{IEEEabrv,biblio}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
%\begin{thebibliography}{1}
%
%\bibitem{IEEEhowto:kopka}
%H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
%  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.
%
%\end{thebibliography}




% that's all folks
\end{document}


