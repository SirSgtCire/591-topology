\documentclass[conference]{IEEEtran}

\usepackage{hyperref}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{You Decide}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations

\author{\IEEEauthorblockN{Alan Cleary\IEEEauthorrefmark{1},
Eric DiGiovine\IEEEauthorrefmark{2},
Daniel Salinas\IEEEauthorrefmark{3} and
Killian Smith\IEEEauthorrefmark{4}}
\IEEEauthorblockA{Department of Computer Science,\\
Montana State University\\
Bozeman, MT 59717\\
Email: \IEEEauthorrefmark{1}http://alancleary.github.io/\#contact,
\IEEEauthorrefmark{2}edigiovine75@gmail.com,
\IEEEauthorrefmark{3}daniel.salinas@msu.montana.edu,\\
\IEEEauthorrefmark{4}vikingsheepman@gmail.com}}

% conference papers do not typically use \thanks and this command
% is locked out in conference mode. If really needed, such as for
% the acknowledgment of grants, issue a \IEEEoverridecommandlockouts
% after \documentclass

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
%\author{\IEEEauthorblockN{Michael Shell\IEEEauthorrefmark{1},
%Homer Simpson\IEEEauthorrefmark{2},
%James Kirk\IEEEauthorrefmark{3}, 
%Montgomery Scott\IEEEauthorrefmark{3} and
%Eldon Tyrell\IEEEauthorrefmark{4}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}School of Electrical and Computer Engineering\\
%Georgia Institute of Technology,
%Atlanta, Georgia 30332--0250\\ Email: see http://www.michaelshell.org/contact.html}
%\IEEEauthorblockA{\IEEEauthorrefmark{2}Twentieth Century Fox, Springfield, USA\\
%Email: homer@thesimpsons.com}
%\IEEEauthorblockA{\IEEEauthorrefmark{3}Starfleet Academy, San Francisco, California 96678-2391\\
%Telephone: (800) 555--1212, Fax: (888) 555--1212}
%\IEEEauthorblockA{\IEEEauthorrefmark{4}Tyrell Inc., 123 Replicant Street, Los Angeles, California 90210--4321}}




% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
%\begin{abstract}
%The abstract goes here.
%\end{abstract}

% no keywords




% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}

In our Initial Investigation we investigated how different topological techniques may be used for pan-genome analysis and what data we would evaluate our methods on.
In this work we will discuss which methods we have chosen to pursue, what data we have analysed, and our progress thus far.

\section{Data Set}

As discussed in the Initial Investigation, we have used toy data sets during algorithmic development, each of which was tailored to ensure that the algorithms were behaving as intended.
We have also tested our algorithms on the real data set we expressed interest in, that is, the pan-genome for 50 yeast genomes acquired from the Saccharomyces Genome Database (\url{http://www.yeastgenome.org/}).
We used E-SplitMEM\cite{beller2015efficient} to construct the pan-genomes from raw sequence.

\section{Topological Techniques}

In our Initial Investigation we investigated four topological techniques we wanted to pursue: persistent homology, topological density filtration, Fr\'{e}chet Distance, and ridge finding.
Ideally we would pursue all of these techniques but have come to realize that the amount of work this would require is well outside the scope of a semester project.
For this reason we have chosen to pursue persistent homology and Fr\'{e}chet Distance.
Our progress so far is reported below.

\subsection{Persistent Homology}

For a pan-genome (de Bruijn graph), we define the spatial resolutions as the $k$ value used to construct the graph and the persistent features as bubbles, where a \emph{bubble} is a pair of genome paths that have the same start and end vertices but are otherwise vertex-disjoint.

We originally implemented both the \emph{bubble calling} and \emph{path divergence} algorithms from \cite{iqbal2012novo} to identify bubbles that we wanted to compute the persistence of.
Later, we realized that the path divergence algorithm can be extended so that the bubbles identified with the bubble calling algorithm are a subset of the bubbles it identifies.
These modifications have been made and we are now strictly using the path divergence algorithm.

A key observation we made when devising how to apply persistent homology is that rather than dying, bubbles merge.
Specifically, all the bubbles on a given genome will eventually merge into a single arc that consists of the entire genome.
So, we want to capture which bubbles merge, when they merge, and how long any one bubble persists between merges.
To do this we developed an inverted tree data structure.

Each genome in the pan-genome is given its own inverted tree.
The leaves in a tree represent the bubbles that the tree's genome are in.
If a genome is in a bubble multiple times it has a different leaf for each time it is in the bubble.
We grow the tree by artificially varying $k$.
This is done by computing the distance between each bubble in the tree.
When we identify the bubbles that will merge next we add an internal node that becomes the parent of the merging bubbles.
This process is continued until all the bubbles in a tree have merged into a single node.
We are currently debugging and testing the tree construction algorithm.

Looking forward, we are contemplating how to leverage our forest of inverted bubble trees in a biologically meaningful way.
We believe how the bubbles persist will help geneticists better understand the evolutionary nature of the population the graph represents.
For example, a long persisting bubble representing a polymorphic site would be more evolutionarily significant than a short persisting one. 
In the case of a bubble representing a repeat, how long the bubble persists, the rate at which either side of the bubble degrades, and which paths traversing the bubble persist the longest can all be utilized when inferring phylogenies - a hard problem\cite{felsenstein2004inferring}.
We will consult with collaborators (domain experts) at the National Center for Genome Resources (NCGR) during this phase of the project to make sure our methods are sound.

\subsection{Fr\'{e}chet Distance}

For a pan-genome, the curves we are measuring the similarity between are the genome paths through the graph.
Since the curves are defined as orderings of vertices, and so are not continuous, we must use the discrete Fr\'{e}chet distance\cite{agarwal2012computing, eiter1994computing}.
After much deliberation, we have chosen to use the Hamming distance\cite{hamming1950error} as the distance measure between any two nodes in the graph.
It was chosen because it carries more biological meaning than other measures and can be computed efficiently.

Using the Fr\'{e}chet distance, we hope to compute a path through the graph that is closest to all the genome paths through the graph, thus creating a ``centroid'' genome.
We have devised a scheme for computing the centroid genome from the Fr\'{e}chet distance variance curve of each genome path in the graph:
Once the variance curve has been computed for each genome in the graph we will find ``jump points'' between each pair of curves, that is, points in the curves that correspond to nodes that are shared between the paths.
We will then define a new node that all genome paths start at and a new node that all genome paths end at.
Finding the centroid will then be a matter of walking from the start node to the end node in such a way that minimizes that area under the portions of the curves that the path traverses.

There are still many details to be worked out: what existing method for computing the discrete Fr\'{e}chet distance is appropriate for our application, how hard the minimization problem is, and what the biological analogue of the centroid path is.
With regards to the latter, the centroid is a single linear sequence that is the most representative of the graph.
We believe this essentially makes it a non-biased reference genome for the population.
If this is true, it could be used by traditional analyses that require a reference genome.
As with the persistent homology technique, how we will utilize the centroid path and quantify its utility will be a collaboration with the NCGR.

\section{Conclusion}

Our progress thus far is substantial.
The persistent homology work is nearing the point where we can send results from the yeast data to the NCGR for analysis.
Around the same time the design of the Fr\'{e}chet distance methodology should be far enough that we could begin implementation.
We may not finish the Fr\'{e}chet distance work by the end of the semester, but the persistent homology work should be substantial enough to merit submitting to a pear reviewed journal.

% use section* for acknowledgment
\section*{Acknowledgment}

We would like to acknowledge Bodacious.
What a bull.





% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{IEEEabrv,biblio}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
%\begin{thebibliography}{1}
%
%\bibitem{IEEEhowto:kopka}
%H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
%  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.
%
%\end{thebibliography}




% that's all folks
\end{document}


