#! /usr/bin/env python

import sys
import os
import pickle
import graph
import bubbles
import networkx as nx
import persistenceForest as pf


def load_pickle(filename):
    with open(filename, 'rb') as f:
        return pickle.load(f)


if __name__ == "__main__":
    # rudimentary input check
    if len(sys.argv) != 3:
        print "Usage:", sys.argv[0], "<dot file> <fasta file>"
        exit()

    graph_pickle = sys.argv[1]+".graph.pkl"
    starts_pickle = sys.argv[1]+".starts.pkl"
    name_pickle = sys.argv[1]+".names.pkl"
    sequence_pickle = sys.argv[1]+".sequence.pkl"
    g = starts = names = sequence = None
    if os.path.isfile(graph_pickle) and\
       os.path.isfile(starts_pickle) and\
       os.path.isfile(name_pickle) and\
       os.path.isfile(sequence_pickle):
        print "Indulging in pickles..."
        g = load_pickle(graph_pickle)
        starts = load_pickle(starts_pickle)
        names = load_pickle(name_pickle)
        sequence = load_pickle(sequence_pickle)
    else:
        # build the graph
        g, starts, names, sequence = graph.constructGraph(*(sys.argv[1:]))
        # back up to file
        print "Canning pickles..."
        pickle.dump(g, open(graph_pickle, "wb"))
        pickle.dump(starts, open(starts_pickle, "wb"))
        pickle.dump(names, open(name_pickle, "wb"))
        pickle.dump(sequence, open(sequence_pickle, "wb"))

    print "sequence:", sequence
    graph.printGraph(g, sequence)

    # identify bubbles
    print "Finding bubbles..."
    bubbles, uid = bubbles.bubbleMiner(g, starts)
    print "    Found", len(bubbles), " bubbles"
    for b in bubbles:
        print b
    print "Constructing inverted bubble tree forest"
    persistence = pf.BubblePersistence(g, bubbles)
    persistence.printForest()
    print "Done!"

    # profit
