import matplotlib.pyplot as plt
from matplotlib import collections



#-- Preprocess data into a format readable by
#-  the barcode generator. 
def preprocess(data):
    pass



#-- Generate a barcode chart for bubbles
def make_barcode(title, data, img_out):
    segments = []
    max_k = 6
    for i in range(0,len(data)):
        for datum in data[i]:
	    segments.append([(datum[0],i),
                             (datum[1],i)])
    barcodes = collections.LineCollection(segments, linewidths=2)
    fig, ax = plt.subplots()
    ax.add_collection(barcodes)
    plt.title(title)
    plt.xlabel('k-value')
    plt.ylabel('Bubble ID')
    plt.yticks(range(0,len(data)))
    plt.xticks(range(0,max_k+1))
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.autoscale()
    ax.margins(0.1)
    plt.show()



#-- Testing! --
data = [[(0,1),(3,5)],
        [(0,5)],
        [(0,2),(3,4),(5,6)]]

title = 'Test Barcode'

img = 'test_barcode.png'


make_barcode(title, data, img)
