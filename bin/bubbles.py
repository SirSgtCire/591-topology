# implementations of the variant calling algorithms from Cortex

import copy


# helper function that creates bubbles
def __bubbleFactory__(uid, b, e, a1, a2, cycle=False):
    bubble = {
        "uid"   : uid,
        "start" : b,
        "end"   : e,
        "cycle" : cycle
    }
    bubble["arc1"] = a1
    bubble["arc2"] = a2
    return bubble


# identifies "clean" bubbles
# clean bubble - a bubble whose arcs have in/out-degree 1
# assumes g is a compressed de Bruijn graph
def bubbleCalling(g, uid=0):
    bubbles = []
    marked = set()

    # iterate all the nodes in the graph
    for n in g.nodes_iter():
        # no need to expand if already part of bubble
        if n not in marked:
            arcs = {}
            # iterate out edges
            for n1, n2 in g.out_edges_iter(n):
                # n1 = n
                if g.in_degree(n2) == 1 and g.out_degree(n2) == 1 and\
                   n2 not in marked:
                    # which genomes could traverse this path?
                    path_ids = set(g.node[n1]["paths"].keys())\
                        .intersection(set(g.node[n2]["paths"].keys()))
                    # do any actually traverse the path?
                    candidates = {}
                    for p in path_ids:
                        for n2_num in g.node[n2]["paths"][p]:
                            for n1_num in g.node[n1]["paths"][p]:
                                if n2_num-n1_num == 1:
                                    candidates.setdefault(p, set()).add(n2_num)
                    # which genomes could traverse this path?
                    for n21, n22 in g.out_edges_iter(n2):
                        path_ids = set(candidates.keys())\
                            .intersection(set(g.node[n22]["paths"].keys()))
                    # do any actually traverse the path?
                    for p in path_ids:
                        for n22_num in g.node[n22]["paths"][p]:
                            for n2_num in candidates[p]:
                                if n22_num-n2_num == 1 and n != n22:
                                    # save as potential bubble arc
                                    arcs.setdefault((n, n22), {})\
                                        .setdefault(n2, {})\
                                        .setdefault(p, set())\
                                        .add((n22_num-2, n22_num))

            # iterate in edges
            for n1, n2 in g.in_edges_iter(n):
                # n2 = n
                if g.in_degree(n1) == 1 and g.out_degree(n1) == 1 and\
                   n1 not in marked:
                    # which genomes could traverse this path?
                    path_ids = set(g.node[n1]["paths"].keys())\
                        .intersection(set(g.node[n2]["paths"].keys()))
                    # do any actually traverse the path?
                    candidates = {}
                    for p in path_ids:
                        for n2_num in g.node[n2]["paths"][p]:
                            for n1_num in g.node[n1]["paths"][p]:
                                if n2_num-n1_num == 1:
                                    candidates.setdefault(p, set()).add(n1_num)
                    # which genomes could traverse this path?
                    for n11, n12 in g.in_edges_iter(n1):
                        path_ids = set(candidates.keys())\
                            .intersection(set(g.node[n11]["paths"].keys()))
                    # do any actually traverse the path?
                    for p in path_ids:
                        for n11_num in g.node[n11]["paths"][p]:
                            for n1_num in candidates[p]:
                                if n1_num-n11_num == 1 and n != n11:
                                    # save as potential bubble arc
                                    arcs.setdefault((n, n11), {})\
                                        .setdefault(n1, {})\
                                        .setdefault(p, set())\
                                        .add((n11_num+2, n11_num))

            # is there a bubble between any start and end nodes?
            for end_points in arcs:
                # is there more than one arc between the end points?
                arc_nodes = arcs[end_points].keys()
                if len(arc_nodes) > 1:
                    begin, end = end_points
                    # every pair of arc nodes form a bubble
                    for i in range(len(arc_nodes)-1):
                        marked.add(arc_nodes[i])
                        for j in range(i+1, len(arc_nodes)):
                            b = __bubbleFactory__(uid, begin, end,
                                    arcs[end_points][arc_nodes[i]],
                                    arcs[end_points][arc_nodes[j]])
                            bubbles.append(b)
                            uid += 1

    return bubbles, uid
                            


def pathDivergence(g, starts, uid=0):
    bubble_bin = {}
    bubbles = []

    # helper function for defining bubble points
    def initBubblePoint(bubble_points, p_id):
        bubble_points.setdefault(p_id, {"diverge":[], "converge":[]})

    # helper functions for adding to a bubble points dict
    def addConvergePoint(bubble_points, p_id, n_id, rel_n_id):
        bubble_points[p_id]["converge"].append((n_id, rel_n_id))

    def addDivergePoint(bubble_points, p_id, n_id, rel_n_id):
        bubble_points[p_id]["diverge"].append((n_id, rel_n_id))

    # helper function that finds the next node in the graph on a given path
    def findNextNode(path, prev, prev_path_id):
        for n1, n2 in g.out_edges_iter(prev):
            paths = g.node[n2]['paths']
            if path in paths:
                path_node_ids = [x for x in paths[path] if x > prev_path_id]
                if path_node_ids and prev_path_id+1 in path_node_ids:
                    return n2
        return None

    # identify divergent bubbles relative to each path in the graph
    for ref in range(len(starts)):
        prev = None
        u = starts[ref]
        ref_u = 0
        previously_on = {}
        bubble_points = {}

        # traverse the path
        while u is not None:
            v = None
            currently_on = {}
            for p in g.node[u]['paths']:
                currently_on[p] = g.node[u]['paths'][p].copy()
                for rel_u in currently_on[p]:
                    # did this path traverse the previous node on u's path?
                    if p in previously_on and rel_u-1 in previously_on[p]:
                        previously_on[p].discard(rel_u-1)
                        if len(previously_on[p]) == 0:
                            del previously_on[p]
                    # which paths converged
                    else:
                        initBubblePoint(bubble_points, p)
                        addConvergePoint(bubble_points, p, u, rel_u)
            # which paths diverged
            for p in previously_on:
                initBubblePoint(bubble_points, p)
                for rel_u in previously_on[p]:
                    addDivergePoint(bubble_points, p, prev, rel_u)
            # find the next node on the path
            v = findNextNode(ref, u, ref_u)
            # traverse to the next node
            prev = u
            u = v
            ref_u += 1
            previously_on = currently_on

        # bin the arcs so multiple paths can traverse a single arc in a bubble
        for p, points in bubble_points.iteritems():
            # sort the converge/diverge points by the relative ids
            points["diverge"].sort(key=lambda ids: ids[1])
            points["converge"].sort(key=lambda ids: ids[1])
            i = j = 0
            while i != len(points["diverge"]) and j != len(points["converge"]):
                # if this is a valid bubble arc
                if points["converge"][j][1] > points["diverge"][i][1]:
                    # where the arc begins and ends
                    begin = points["diverge"][i]
                    end = points["converge"][j]
                    # traverse the arc and find the graph node ids
                    u = begin[0]
                    path_node_id = begin[1]
                    arc_nodes = []
                    while u != end[0]:
                        arc_nodes.append(u)
                        u = findNextNode(p, u, path_node_id)
                        path_node_id += 1
                    arc_nodes = arc_nodes[1:]
                    # orient the arc relative to the proper bubble ordering
                    if begin[0] > end[0]:
                        begin, end = end, begin
                        arc_nodes = arc_nodes[::-1]
                    arc_key = tuple(arc_nodes)
                    # add to the bubble bin!
                    bubble_key = (begin[0], end[0])
                    bubble_bin.setdefault(bubble_key, {})\
                              .setdefault(arc_key, {})\
                              .setdefault(p, set())\
                              .add((begin[1], end[1]))
                    i += 1
                else:
                    j += 1

    # make some freakin bubbles!
    for (begin, end), arcs in bubble_bin.iteritems():
        # each pair of arcs makes a bubble
        arcs_as_node_sets = arcs.keys()
        for i in range(len(arcs_as_node_sets)-1):
            arc1 = arcs[arcs_as_node_sets[i]]
            for j in range(i+1, len(arcs_as_node_sets)):
                arc2 = arcs[arcs_as_node_sets[j]]
                b = __bubbleFactory__(uid, begin, end, arc1, arc2)
                bubbles.append(b)
                uid += 1

    return bubbles, uid


def bubbleMiner(g, starts, uid=0):
    bubble_bin = {}
    bubbles = []


    # helper function for defining bubble points
    def initBubblePoint(bubble_points, p_id):
        bubble_points.setdefault(p_id, {"diverge":[], "converge":[]})


    # helper functions for adding to a bubble points dict
    def addConvergePoint(bubble_points, p_id, n_id, ref_n_id, rel_n_id):
        bubble_points[p_id]["converge"].append((n_id, ref_n_id, rel_n_id))


    def addDivergePoint(bubble_points, p_id, n_id, ref_n_id, rel_n_id):
        bubble_points[p_id]["diverge"].append((n_id, ref_n_id, rel_n_id))


    # helper function that finds the next node in the graph on a given path
    def findNextNode(path, prev, prev_path_id):
        for n1, n2 in g.out_edges_iter(prev):
            paths = g.node[n2]['paths']
            if path in paths:
                path_node_ids = [x for x in paths[path] if x > prev_path_id]
                if path_node_ids and prev_path_id+1 in path_node_ids:
                    return n2
        return None


    # actually constructs the arc and bins it
    def binArc(p, begin, end, path):
        # fill in the arc nodes between begin and end as traversed by p
        u, stop, path_node_id = (begin[0], end[0], int(begin[path]))\
            if begin[path] < end[path] else (end[0], begin[0], int(end[path]))
        arc = []
        while u != stop:
            arc.append(u)
            u = findNextNode(p, u, path_node_id)
            path_node_id += 1
        arc.append(stop)
        # add the filled arc to the bubble bin!
        n1, n2 = begin[0], end[0]
        r1, r2 = begin[path], end[path]
        if n1 > n2:
            n1, n2 = n2, n1
            r1, r2 = r2, r1
        bubble_bin.setdefault((n1, n2), {})\
                  .setdefault(tuple(arc), {})\
                  .setdefault(p, set())\
                  .add((r1, r2))


    # identifies arcs and adds them to the bubble bin
    def binArcs(ref, p, points, path):
        # sort the converge/diverge points by the relative ids
        points["diverge"].sort(key=lambda ids: ids[path])
        points["converge"].sort(key=lambda ids: ids[path])
        i = j = 0
        while i != len(points["diverge"]) and j != len(points["converge"]):
            # if this is a valid bubble arc
            if points["converge"][j][path] > points["diverge"][i][path]:
                # where the arc begins and ends
                begin = points["diverge"][i]
                end = points["converge"][j]
                # fill in the nodes between the beginning and end nodes
                binArc(ref, begin, end, 1)
                binArc(p, begin, end, 2)
                i += 1
            else:
                j += 1


    # treat each path as a reference
    for ref in range(len(starts)):
        prev = None
        u = starts[ref]
        ref_u = 0
        previously_on = {}
        bubble_points = {}

        # traverse each node u in ref sequentially:
        while u is not None:
            v = None
            currently_on = {}
            for p in g.node[u]['paths']:
                currently_on[p] = g.node[u]['paths'][p].copy()
                for rel_u in currently_on[p]:
                    # did this path traverse the previous node on u's path?
                    if p in previously_on and rel_u-1 in previously_on[p]:
                        previously_on[p].discard(rel_u-1)
                        if len(previously_on[p]) == 0:
                            del previously_on[p]
                    # which paths converged
                    else:
                        initBubblePoint(bubble_points, p)
                        addConvergePoint(bubble_points, p, u, ref_u, rel_u)
            # which paths diverged
            for p in previously_on:
                initBubblePoint(bubble_points, p)
                for rel_u in previously_on[p]:
                    addDivergePoint(bubble_points, p, prev, ref_u-1, rel_u)
            # find the next node on the path
            v = findNextNode(ref, u, ref_u)
            # traverse to the next node
            prev = u
            u = v
            ref_u += 1
            previously_on = currently_on

        # bin the arcs so multiple paths can traverse a single arc in a bubble
        for p, points in bubble_points.iteritems():
            # sort the converge/diverge points by the reference ids
            binArcs(ref, p, points, 1)
            # sort the converge/diverge points by the relative ids
            binArcs(ref, p, points, 2)


    #bubble_bin.setdefault((n1, n2), {})\
    #          .setdefault(tuple(arc), {})\
    #          .setdefault(p, set())\
    #          .add((r1, r2))

    # make some freakin bubbles!
    for (begin, end), arcs in bubble_bin.iteritems():
        # each pair of arcs makes a bubble
        ordered_arc_nodes = arcs.keys()
        for i in range(len(ordered_arc_nodes)-1):
            arc1 = arcs[ordered_arc_nodes[i]]
            for j in range(i+1, len(ordered_arc_nodes)):
                # check if the arcs form a simple cycle
                cycle = ordered_arc_nodes[i][0] != ordered_arc_nodes[j][0]
                arc2 = arcs[ordered_arc_nodes[j]]
                b = __bubbleFactory__(uid, begin, end, arc1, arc2, cycle)
                bubbles.append(b)
                uid += 1

    return bubbles, uid
