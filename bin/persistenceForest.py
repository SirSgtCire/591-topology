import sys, pprint, Queue


# constructs a forest of persistence trees from the given set of bubbles
class BubblePersistence():
    def __init__(self, g, bubbles):
        genomes_bubbles = {}
        self.forest = {}
        # bin the bubbles by genome
        bubbleBin = {}  # key = geneome_id, value = list of bubbles
        for b in bubbles:
            for p in b['arc1']:
                bubbleBin.setdefault(p, []).append(b)
            for p in b['arc2']:
                bubbleBin.setdefault(p, []).append(b)
        # create a persistence tree for each bubble
        for p, bubbles in bubbleBin.iteritems():
            ptree = PersistenceTree(g, p, bubbles)
            self.forest[p] = ptree


    def kernelHomology(self):
        pass


    def printForest(self):
        for p, ptree in self.forest.iteritems():
            print p, "tree"
            ptree.printTree()


# an inverted bubble tree from which bubble persistence can be computed
class PersistenceTree():
    def __init__(self, g, p, bubbles):
        # generate the leaves for the tree
        self.leaves, uid = self.getLeaves(g, p, bubbles)
        # construct the tree
        self.mergeBubbles(uid)


    # creates tree nodes
    def nodeFactory(self, name, uid, start, end, children=None):
        node = {'name'        : name,
                'uid'         : uid,
                'start'       : start,
                'end'         : end,
                'persistence' : 0,
                'parents'     : {},  # leaves can have multiple parents
                'children'    : children}
        return node


    # generates leaves for each of the genome's bubbles
    def getLeaves(self, g, p, bubbles):
        leaves = {}
        uid = 0

        # identifies pairwise bubbles
        def pairwiseBubbles(b, arc, complement, uid):
            # for each time p traverse arc
            for (r1, r2) in b[arc][p]:
                id1, id2 = b['start'], b['end']
                if r1 > r2:
                    r1, r2 = r2, r1
                    id1, id2 = id2, id1
                n1, n2 = g.node[id1], g.node[id2]
                start = n1['map'][p][r1]
                end = n2['map'][p][r2]
                # the start and end locations depend on if the arcs have the
                # same orientation
                if b['cycle']:
                    end += n2['length']
                else:
                    start += n1['length']
                # check if an insertion has occurred
                if start >= end:
                    start = (start+end)/2
                    end = start+1
                leaf = self.nodeFactory(set([b['uid']]), uid, start, end)
                uid += 1
                # bubbles formed with complement arc are binned as leaves
                for p2 in b[complement]:
                    leaves.setdefault(p2, []).append(leaf)
            return uid

        # bin leaves by genome
        for b in bubbles:
            # make leaves for the first arc
            if p in b['arc1']:
                uid = pairwiseBubbles(b, 'arc1', 'arc2', uid)
            # make leaves for the second arc
            if p in b['arc2']:
                uid = pairwiseBubbles(b, 'arc2', 'arc1', uid)

        return leaves, uid


    # boilerplate for creating internal nodes
    def createInternalNode(self, p, merge, uid, name):
        # create the new internal node
        start = min(map(lambda n: n['start'], merge))
        end = max(map(lambda n: n['end'], merge))
        node = self.nodeFactory(name, uid, start, end, merge)
        # update its children appropriately
        for n in merge:
            n['parents'][p] = node
        return node


    # given an arbitrary tree node and genome, finds the current root
    def findRoot(self, n, p):
        if p not in n['parents']:
            return n
        return self.findRoot(n['parent'][p], p)


    # iterates genome leafs groups, ordering them and merging overlaps
    def preprocessLeaves(self, uid):
        nodes = {}
        # operate on each genome's disjoint tree
        for p, p_leaves in self.leaves.iteritems():
            # sort the leaves by their genomic start position
            p_leaves.sort(key=lambda l: l['start'])
            # merge leaves that are already overlapping
            i, nodes[p] = 0, []
            while i < len(p_leaves)-1:
                l1 = p_leaves[i]
                merge = [l1]
                end = l1['end']
                for l2 in p_leaves[i+1:]:
                    if l2['start'] < end:
                        merge.append(l2)
                        if l2['end'] > end:
                            end = l2['end']
                    else:
                        break
                if len(merge) > 1:
                    name = set.union(*map(lambda n: n['name'], merge))
                    nodes[p].append(self.createInternalNode(p, merge, uid, name))
                    uid += 1
                else:
                    nodes[p].append(l1)
                i += len(merge)
        return nodes, uid


    def mergeBubbles(self, uid):
        # merge leaves that are already overlapping and order nodes for merging
        initialNodes, uid = self.preprocessLeaves(uid)
        # operate on each genome's disjoint tree
        for p, nodes in initialNodes.iteritems():
            # compute genomic distance between all neighboring nodes
            distances = []
            for i in range(len(nodes)-1):
                n1, n2 = nodes[i:i+2]
                distances.append((n2['start']-n1['end']+1, n1, n2))
            distances.sort(key=lambda d: d[0])
            # apply miracle-grow to tree
            i = 0
            while i < len(distances):
                # the next to merge
                d = distances[i][0]
                merge = {}
                # find the nodes that are actually merging
                left = self.findRoot(distances[i][1], p)
                merge[left['uid']] = left
                right = self.findRoot(distances[i][2], p)
                merge[right['uid']] = right
                # any node pairs with the same distance should be merged too
                j = i+1
                while j < len(distances) and distances[j][0] == d:
                    left = self.findRoot(distances[j][1], p)
                    merge[left['uid']] = left
                    right = self.findRoot(distances[j][2], p)
                    merge[right['uid']] = right
                # update the persistence of each node being merged
                for n_uid, n in merge.iteritems():
                    n['persistence'] = d
                # actually merge the nodes
                name = set.union(*map(lambda n: n['name'], merge.values()))
                self.createInternalNode(p, merge.values(), uid, name)
                # prepare for the next iteration
                uid += 1
                i += len(merge)


    def printTree(self):
        def recur(n, p, seen):
            if n['uid'] not in seen:
                seen[n['uid']] = n
                print '\tname:', n['name']
                print '\tuid:', n['uid']
                print '\tstart:', n['start']
                print '\tend:', n['end'],
                print '\tpersistence:', n['persistence']
                if n['children'] is not None:
                    print '\tchildren:', map(lambda c: c['uid'], n['children'])
                else:
                    print '\tchildren: none'
                if p in n['parents']:
                    print "\tparent:", n['parents'][p]['uid'], '\n'
                    recur(n['parents'][p], p, seen)
                else:
                    print "\tparent: is root!\n"
        for p, p_leaves in self.leaves.iteritems():
            seen = {}
            print p, "disjoint tree"
            for l in p_leaves:
                recur(l, p, seen)
        #pp = pprint.PrettyPrinter()
        #for tree in forest:
        #    print 'Genome-'+str(tree)+': '
        #    pp.pprint(forest[tree].leaves)
        #    print "\n\n"  
