import networkx as nx
import pygraphviz as pgv
import re


def constructGraph(dot_file, fa_file):
    # build the networkx graph for the dot file
    print "Loading dot file..."
    parsed_graph = pgv.AGraph(dot_file)
    print "Building graph..."
    g = nx.DiGraph(parsed_graph)

    # generate a list of sequence names and intervals
    print "Loading fasta file..."
    names, intervals, starts, sequence = [], [], [], []
    with open(fa_file) as f:
        length = 0
        i = -1
        for line in f:
            line = line.rstrip('\n')
            if line.startswith('>'):
                starts.append('0')
                sequence.append('')
                i += 1
                try:
                    length += 1  # account for delimiting character
                    name = re.match('\w+', line[1:]).group()
                    names.append(name)
                    intervals.append(length)
                except:
                    print 'Invalid name on line', f.tell(), 'of', fa_file+':'
                    print line
                    exit()
            else:
                sequence[i] += line
                length += len(line)
        intervals.append(length+1)

    # parse the node length and genomes from each node label
    print "Setting graph attributes..."
    print "    Labeling nodes..."
    for n, data in g.nodes_iter(data=True):
        pos, length = data['label'].split(':')
        del g.node[n]['label']
        g.node[n]['length'] = int(length)
        g.node[n]['paths'] = {}
        g.node[n]['samples'] = {}
        g.node[n]['map'] = {}
        positions = map(int, pos.split(','))
        positions.sort()
        for p in positions:
            for i in range(len(intervals)-1):
                if p >= intervals[i] and p < intervals[i+1]:
                    if p == intervals[i]:
                        starts[i] = n
                    p -= intervals[i]
                    g.node[n]['samples'].setdefault(i, set()).add(p)
                    break

    # prepare edges for path counts
    print "    Embedding paths..."
    for n1, n2 in g.edges_iter():
        g[n1][n2]['paths'] = {}

    # sequentially number the nodes along each sample's path
    for i in range(len(starts)):
        u = starts[i]
        p = min(g.node[u]['samples'][i])
        num = 0
        # traverse the path
        while u is not None:
            # populate the genome-node-num-to-genome-position map
            g.node[u]['map'].setdefault(i, {})[num] = p
            # what number is u on i's path
            g.node[u]['paths'].setdefault(i, set()).add(num)
            # find the next node on the path
            v = None
            vp = None
            for n1, n2 in g.out_edges_iter(u):
                s = g.node[n2]['samples']
                if i in s:
                    values = [x for x in s[i] if x > p]
                    if values:
                        n2p = min(values)
                        if vp is None or n2p < vp:
                            v = n2
                            vp = n2p
            # add path to edge
            if v:
                if i in g[u][v]['paths']:
                    g[u][v]['paths'][i] += 1
                else:
                    g[u][v]['paths'][i] = 1
            # traverse to the next node
            u = v
            p = vp
            num += 1

    return g, starts, names, sequence


def printGraph(g, sequence):
    print "nodes:"
    for n in g.nodes_iter():
        node = g.node[n]
        print n, ":", node
        for p in node['samples']:
            print '\t', p, ":"
            for i in node['samples'][p]:
                print '\t\t', sequence[p][i:i+node['length']]
    for n1, n2 in g.edges_iter():
        print n1, "->", n2
